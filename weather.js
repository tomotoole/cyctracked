//weather Module
'use strict'
var request = require('request')

// app id for openweathermap api
const appID = "3ce0c07f08c4505df54c0e6fd74ff47c"
//base url that 3rd party uses
const urlBase = "http://api.openweathermap.org/data/2.5/"


/*exports.Current = function(city, country, data){
    new Promise((resolve, reject) =>{
        const url = urlBase + "weather"
        const query_string = {q: city + "," + country,units: "metric", appid:appID}
        request.get({url: url, qs: query_string}, function(err, res, body) {
            //console.log({url: url, qs: query_string})
            if (err) {
                reject({code:500, response:{status:'error', message:'current weather failed', data:err}})
            }
            const json = JSON.parse(body)
            var weatherObj = {
                name:json.name,
                country:json.sys.country,
                wind: json.wind,
                description: json.weather[0].description 
                
            }
            console.log(weatherObj)
            resolve({code:200, response:{status: 'sucesss', message: weatherObj}})
        }).then(data(resolve))
    })
    
}*/

exports.Current = function(city, country, data){
        //set the url to the base plus weather to get the current weather
        const url = urlBase + "weather"
        //construct the query string adding the appid to the end  
        const query_string = {q: city + "," + country,units: "metric", appid:appID}
        //send the get request to  3rd party api
        request.get({url: url, qs: query_string}, function(err, res, body) {
            if (err) {
                data({code:500, response:{status:'error', message:'current weather failed', data:err}})
            }
            
            const json = JSON.parse(body)

            //convert the Unix time to local time split the time and date
            var fulldate = new Date(json.dt*1000).toLocaleString()
            var dateSplit = fulldate.split(',')
            const date = dateSplit[0]
            const time = dateSplit[1]
            
            //set data that is to be sent from the 3rd party api
            const weatherObj = {
                name:json.name,
                country:json.sys.country,
                wind: json.wind,
                temp: json.main.temp,
                description: json.weather[0].description, 
                iconUrl: "http://openweathermap.org/img/w/" + json.weather[0].icon + ".png",
                time: time.trim(),
                date: date
            }
            //send wanted data back
            data({code:200, response:{status: 'sucesss', message: weatherObj}})
        })
}


exports.FiveDay = (city, country, callback)=>{
    console.log("5DayForecast")
    //set url to go to forecast api
    const url = urlBase + "forecast"
    const query_string = {q: city + "," + country,units: "metric", appid:appID}
    //send request to 3rd party
    request.get({url: url, qs: query_string}, function(err, res, body) {
        if(err){
           callback({code: 400, response:{status: 'error', message:'current weather failed', data:err}})
        }
        const json = JSON.parse(body)
        //create object to store data wanted
        const list = json.list
        let city = json.city
        let daylist = list.map(element => ({
            date: convertDt(element.dt),
            temp: element.main.temp + 'C',
            weatherDesc: element.weather[0].description,
            //image link for icon
            iconUrl: "http://openweathermap.org/img/w/" + element.weather[0].icon + ".png",
            windSpeed: element.wind.speed
        }))
        //combine city with the list 
        const weatherObj = {
            name: city.name,
            list: daylist
        }
        //send wanted data back
        callback({code: 200, response:{status:'success', message: weatherObj}})
    })
}
    
// get daily forcat for x many days
exports.Daily = (city, country, days, callback)=>{
    //set url to point to daily forcast on 3rd party
    const url = urlBase + "forecast/daily"
    const query_string = {q: city+ "," + country, units: "metric",cnt: days, appid:appID}
    //send get request
    request.get({url: url, qs: query_string}, function(err, res, body) {
        if (err) {
        callback({code:500, response:{status:'error', message:'daily weather forcast failed', data:err}})
        }
        
        const json = JSON.parse(body)
        // make object for wanted data
        let list = json.list
        let city = json.city
        let dailyList = list.map(element =>({
            date: convertDt(element.dt), 
            dayTemp: element.temp.day +"C",
            nightTemp: element.temp.night+"C",
            minTemp: element.temp.min+"C",
            maxTemp: element.temp.max+"C",
            weatherDesc: element.weather[0].description,
            //image link for icon
            iconUrl: "http://openweathermap.org/img/w/" + element.weather[0].icon + ".png",
            windSpeed: element.speed,
            windDirection: element.deg
        }))
        
        const weatherObj = {
            name: city.name,
            list: dailyList
        }
        //send wanted data
        callback({code:200, response:{status:'success', message:weatherObj}})
    })
}

function convertDt(dt) {
    // body...
    var fulldate = new Date(dt*1000).toLocaleString()
    var dateSplit = fulldate.split(',')
    const date = dateSplit[0]
    const time = dateSplit[1]
    return {date:date, time:time}
}

/*function urlString (weatherType){
    return new Promise((resolve, reject)=>{
        resolve(urlBase + weatherType)
    })
}

function currentRequest(url, query_string, callback) {
    console.log("currentRequest")
    request.get({url: url, qs: query_string}, function(err, res, body) {
        
        if (err) {
            console.log("error")
            callback({code:500, response:{status:'error', message:'current weather failed', data:err}})
        }
        const json = JSON.parse(body)
        var weatherObj = {
            name:json.name,
            country:json.sys.country,
            wind: json.wind,
            description: json.weather[0].description 
        }
        console.log("weatherObj = " + weatherObj)
        callback({code:200, response:{status: 'sucesss', message: weatherObj}})
    })
}*/

