var fs = require('fs')
var rewire = require('rewire')
var weather = rewire("../weather.js")


describe('Get current forecast', ()=>{
    /*weather.__set__('Current', (city, country, callback)=>{
        console.log("MOCK Current")
        fs.readFile('spec/data/leam_current.json', 'utf8', (err,data)=>{
            if(err) {
                callback({code:500, response:{status: 'failed', message: err}})
            }
            console.log("data:" +data.toString())
            callback({code:200, response:{status: 'sucesss', message: data}})
        })
        //console.log("currentRequest " + current)
        //return (current)
    })*/
        
    it('get current forcast for leamington', done =>{
        
        weather.Current("leamington", "uk", data =>{
        console.log("current test: " + JSON.stringify(data))
        expect(data.response.message.name).toBe("Royal Leamington Spa")
        expect(data.code).toBe(200)
        done()
        })
        
    })
})

describe('Get five day forecast', ()=>{
    /*weather.__set__('FiveDay', (city, country, callback)=>{
        console.log("MOCK Current")
        fs.readFile('spec/data/leam_five.json', 'utf8', (err,data)=>{
            if(err) {
                callback({code:500, response:{status: 'failed', message: err}})
            }
            console.log("data:" +data.toString())
            callback({code:200, response:{status: 'sucesss', message: data}})
        })
        //console.log("currentRequest " + current)
        //return (current)
    })*/
        
    it('get 5 day forcast for leamingtonnpm test', done =>{
        
        weather.FiveDay("leamington", "uk", data =>{
        console.log("five test: " + JSON.stringify(data))
        console.log(data.response.message.list.length)
        expect(data.response.message.name).toBe("Royal Leamington Spa")
        expect(data.code).toBe(200)
        done()
        })
        
    })
})

describe('Get 10 day forecast', ()=>{
    /*weather.__set__('FiveDay', (city, country, callback)=>{
        console.log("MOCK Current")
        fs.readFile('spec/data/leam_five.json', 'utf8', (err,data)=>{
            if(err) {
                callback({code:500, response:{status: 'failed', message: err}})
            }
            console.log("data:" +data.toString())
            callback({code:200, response:{status: 'sucesss', message: data}})
        })
        //console.log("currentRequest " + current)
        //return (current)
    })*/
        
    it('get 10 day forcast for Leamington', done =>{
        
        weather.Daily("leamington", "uk", 10, data =>{
        console.log("ten test: " + JSON.stringify(data))
        expect(data.response.message.list.length).toBe(10)
        expect(data.response.message.name).toBe("Royal Leamington Spa")
        expect(data.code).toBe(200)
        done()
        })
        
    }),
    it('get 17 day forcast for Leamington', done =>{
        
        weather.Daily("leamington", "uk", 17, data =>{
        console.log("ten test: " + JSON.stringify(data))
        expect(data.response.message.list[17]).toBe(undefined)
        expect(data.response.message.name).toBe("Royal Leamington Spa")
        expect(data.code).toBe(200)
        done()
        })
        
    })
})