'use strict'
// set the require 
var bcrypt = require('bcrypt')
var mongoose = require('mongoose')
const schema = require('./schema.js')
//database name
var database = 'apiData'
// setting server and connecting
const server = 'mongodb://'+process.env.IP+'/'+database
console.log(server)
mongoose.connect(server)
const db = mongoose.connection
// set member schema from schema file
var Member = schema.Member

//adds member to Member collection
exports.addMember = (data, callback)=>{
    console.log("addMember " + JSON.stringify(data))
    //create new Member
    const newMember = new Member({
        FirstName: data.FirstName,
        Surname: data.Surname,
        UserName: data.UserName,
        Password: data.Password,
    })
    //save new member to data base
    newMember.save((err, data)=>{
        if (err) {
            callback({code:412, response:{status:'failed', message:err}})
        }
        callback({code:200, response:{status:'success', message:data}})
    })
}

//find a user based of its username and return what is found
exports.findUser = (userQuery, callback)=>{
    Member.findOne({UserName: userQuery}, (err, data)=>{
        if(err){
            callback({code:404, response:{status:'failed', message:err}})
        }
        callback({code:200, response:{status:'success', message:data}})
    })
}
//check that the user in members and password matches
exports.authpass = (authQuery, callback)=>{
    //set values from auth query
    const user = authQuery.UserName
    const password = authQuery.Password
    //find one user
    Member.findOne({UserName: user}, (err, data)=>{
        if(err){
            callback({code:404, response:{status:'failed', message:err}})
        }
        //set the stored passord and check with query send back response
        const dbpass = data.Password
        if(dbpass != password){
            callback({code:409, response:{status: 'failed', message: 'incorrect password'}})
        }else{
        callback({code:200, response:{status:'success', message:data}})
        }
    })
}

//remove member from Members
exports.removeMember = (user, callback)=>{
    //find based on username and remove
    Member.findOneAndRemove({UserName: user}, (err, data)=>{
        if(err){
            callback({code:404, response:{status:'failed', message:err}})
        }
        callback({code:200, response:{status:'success', message:data}})
    })
}

//update member
exports.updateMember = (user, callback)=>{
    //slip ser into right parts
    const oldfirst = user.OFN
    const oldSur = user.OSN
    const firstname = user.FirstName
    const surname = user.Surname
    
    //find using old names and replace with new send back result
    Member.findOneAndUpdate({FirstName: oldfirst, Surname: oldSur}, {FirstName: firstname, Surname: surname}, (err, data)=>{
        if(err){
            callback({code:404, response:{status:'failed', message:err}})
        }
        callback({code:200, response:{status:'success', message:data}})
    })
}
