var mongoose = require('mongoose')
var Schema = mongoose.Schema

var MemberSchema = new Schema({
    FirstName:{type: String, required: true,},
    Surname:{type:String},
    UserName: {type: String, required: true, unique:true},
    Password:{type: String, required: true},
})

var RideTimeSchema = new Schema({
    MemberID:{type: String},
    Units:{type: String},
    Distance:{type: Number},
    Time:{type: Number},
    Pace:{type: Number},
    Date: {type: Date},
    Place:{type: String},
    Public:{type: Boolean}
})

exports.Member = mongoose.model('Member', MemberSchema)
exports.Times = mongoose.model('RideTime', RideTimeSchema)
