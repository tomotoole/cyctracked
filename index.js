var restify = require('restify')
var server = restify.createServer()

server.use(restify.fullResponse())
server.use(restify.queryParser())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())

var weather = require('./weather.js')
var mongo = require('./mongo.js')

//##########Weather Section##########
server.get('weather/Current', (req, res) => {
    // body...
    const city = req.query.q
    const country = req.query.country
    console.log(city +" "+ country)
    weather.Current(city, country, data =>{
      console.log("index log " + data)
      res.setHeader('content-type', 'application/json');
      res.send(data.code, data.response);
      res.end();
    })
})

server.get('weather/FiveDay', (req, res) => {
    // body...
    const city = req.query.q
    const country = req.query.country

    weather.FiveDay(city, country, data =>{
      console.log(data)
      res.setHeader('content-type', 'application/json')
      res.send(data.code, data.response)
      res.end()
    })
})

server.get('weather/Daily', (req, res) => {
    // body...
    const city = req.query.q
    const country = req.query.country
    const days = req.query.cnt
    //console.log(city +" "+ country + " " + days)
    weather.Daily(city, country, days, data =>{
      console.log(data)
      res.setHeader('content-type', 'application/json');
      res.send(data.code, data.response);
      res.end();
    })
})

//##########Members Section##########
server.post('mongo/addMember', (req, res)=>{
  //set the data recived by req to be used to create new member
  var memberData = req.params
  console.log(memberData)
  mongo.addMember(memberData, data =>{
    console.log(data)
    res.setHeader('content-type', 'application/json')
    res.send(data.code, data.response);
    res.end();
  })
})

server.get('mongo/login', (req, res)=>{
  const user = req.query.u
  const password = req.query.p
  const userData = {UserName: user, Password:password}
  console.log("userData: " + userData)
  mongo.authpass(userData, data=>{
    console.log(data)
    res.setHeader('content-type', 'application/json')
    res.send(data.code, data.response);
    res.end();
  })
})

server.get('mongo/findMember', (req, res)=>{
  const user = req.query.user
  console.log("User: "+user)
  mongo.findUser(user, data=>{
    console.log(data)
    res.setHeader('content-type', 'application/json')
    res.send(data.code, data.response);
    res.end();
  })
})

server.del('mongo/removeMember', (req, res)=>{
  const user = req.params.UserName
  console.log("User: "+user)
  mongo.removeMember(user, data=>{
    console.log(data)
    res.setHeader('content-type', 'application/json')
    res.send(data.code, data.response);
    res.end();
  })
})

server.put('mongo/updateMember', (req, res)=>{
  const user = req.params
  console.log("User: "+user)
  mongo.updateMember(user, data=>{
    console.log(data)
    res.setHeader('content-type', 'application/json')
    res.send(data.code, data.response);
    res.end();
  })
})

var port = process.env.PORT || 8080;
server.listen(port, err => {
  if (err) {
      console.error(err);
  } else {
    console.log('App is ready at : ' + port);
  }
})
